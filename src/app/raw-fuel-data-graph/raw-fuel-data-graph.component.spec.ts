import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RawFuelDataGraphComponent } from './raw-fuel-data-graph.component';

describe('RawFuelDataGraphComponent', () => {
  let component: RawFuelDataGraphComponent;
  let fixture: ComponentFixture<RawFuelDataGraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RawFuelDataGraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RawFuelDataGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
